/// <reference types="Cypress" />

import ClientProfileElements from "../elements/clientProfileElements";
const clientElements = new ClientProfileElements();


class NewClientPage {
    fillCompanyField(companyName) {
        cy.get(clientElements.companyName()).type(companyName);
    }
    fillCompanyWebsiteField(companyWebsite) {
        cy.get(clientElements.companyWebsite()).type(companyWebsite);
    }
    fillCompanyLogoField(companyLogo) {
        cy.get(clientElements.companyLogo()).type(companyLogo);
    }
    fillContactNameField(contactName) {
        cy.get(clientElements.contactName()).type(contactName);
    }
    fillContactPhoneField(contactPhone) {
        cy.get(clientElements.contactPhone()).type(contactPhone);
    }
    fillContactEmailField(contactEmail) {
        cy.get(clientElements.contactEmail()).type(contactEmail);
    }
    clickCreateButton() {
        cy.contains(clientElements.createButton()).click();
    }
    verifyCreatedSuccessfully() {
        cy.on("window:alert", (str) => {
            expect(str).to.equal(`New client successfuly registered!`);
        });
    }
    verifyRequiredFields() {
        cy.on("window:alert", (str) => {
            expect(str).to.equal(`Error: Request failed with status code 400`);
        });
    }
}

export default NewClientPage;
