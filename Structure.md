# Structure
<table>
<tr>
  <th>Navigate</th>
  <td>
      <a href="./../Setup.md">Setup</a> 
    | <a href="./../Execution.md">Execution</a>
  </td>
</tr>
</table>

## Index    
- [Cucumber-json](#cucumber-json)
- [Integration](#integration)
    * [Feature Files](#feature-files)
- [Plugins](#plugins)
- [Reports](#reports)
- [Screenshots](#screenshots)
- [Support](#support)
    * [Body](#body)
    * [Elements](#elements)
    * [Pageobjects](#pageobjects)
    * [Steps](#steps)
- [Videos](#videos)    
            
## Cucumber-json
Where will the cucumber JSON report be generated for each feature file.

## Integration
### Feature Files
In the `.feature`, there are the scenarios for each feature.

To add a new feature just create a `.feature` file corresponding to the feature.

To add a scenario to a feature, just open the `.feature` file and include the desired scenario written in BDD

## Plugins
This folder includes index.js file. This file will be automatically imported every time before the execution of every spec(test) file. Plugins enable you to tap into, modify, or extend the internal behavior of Cypress.
In our case we use it to configure the cucumber.

## Reports
The reports folder where will be generated HTML report

## Screenshots
Cypress comes with the ability to take screenshots, whether you are running via `cypress open or cypress run`, even in CI.

## Support
### Elements
In these files, the elements of the screen are mapped.

Example: `clientProfileElements.js`

* Element:
```
companyName = () => { return 'input[placeholder="Company name"]' }
```

### Pageobjects
In these files the methods that interact on the screen are created.

Example: `clientProfilePage.js`

* Methods:
```
fillCompanyField(companyName) {
        cy.get(clientElements.companyName()).type(companyName);
    }
```

### Body
In this folder, files with the bodies used in API calls are created.

### Steps
In this folder are located the `.js` files that implement the commands of each steps defined in the scenario.

To add a new step, just create a `.js` file corresponding to the functionality.

To add a step to a step_definition, just open the `.js` file and include the desired commands for that step.

Exemplo:
```javascript
When("fill in all fields correctly on the New Client page", () => {
    newClient.fillCompanyField(companyName);
    newClient.fillCompanyWebsiteField(companyWebsite);
    newClient.fillCompanyLogoField(companyLogo);
    newClient.fillContactNameField(contactName);
    newClient.fillContactPhoneField(contactPhone);
    newClient.fillContactEmailField(contactEmail);
    newClient.clickCreateButton();
});
```

## Videos
Cypress records a video for each spec file when running tests during `cypress run`. Videos are not automatically recorded during `cypress open`.
