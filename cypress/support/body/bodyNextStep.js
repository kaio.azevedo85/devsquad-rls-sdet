var BODY_NEXT_STEP = {
  firstName: "Dwight",
  lastName: "Schrute",
  jobDescription: "Manager",
  currentStep: 2
};

var BODY_NEXT_STEP_BLANK = {
  firstName: "",
  lastName: "",
  jobDescription: "",
  currentStep: 0
};

module.exports = {
  BODY_NEXT_STEP,
  BODY_NEXT_STEP_BLANK,
};
