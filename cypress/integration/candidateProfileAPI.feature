Feature: API Candidate Profile

    @get
    Scenario: Validate get candidates
        When send a get request at the endpoint candidates
        Then must visualize the candidates correctly

    @get
    Scenario: Validate get candidate
        When send a get request at the endpoint candidate by first name "Jensen"
        Then must visualize the candidates correctly

    @get
    Scenario: Validate get candidate
        When send a get request at the endpoint candidate by last name "Schumm"
        Then must visualize the candidates correctly

    @post
    Scenario: Validate required fields in post body
        When send a post request with blank fields at the endpoint candidate
        Then should see status and message error

    @post
    Scenario: Validate new candidate profile creation
        When send a post request at the endpoint candidate
        Then should see the created candidate response correctly