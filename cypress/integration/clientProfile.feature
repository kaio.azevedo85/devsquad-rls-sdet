Feature: Create client profile

    Scenario: Validate screen fields
        Given access to the RLS System website
        When access the New Client page
        Then must visualize the fields correctly

    @client
    Scenario: Validate required fields
        Given access to the RLS System website
        When access the "New Client" page
        And don't fill all the fields on the New Client page
        Then should see required fields error message

    @client
    Scenario: Validate new customer creation
        Given access to the RLS System website
        When access the "New Client" page
        And fill in all fields correctly on the New Client page
        Then should see the created client correctly