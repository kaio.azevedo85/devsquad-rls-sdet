const url = Cypress.config("apiUrl");

const { BODY_CLIENT, BODY_CLIENT_BLANK } = require("../body/bodyClient");

When("send a get request at the endpoint clients", () => {
    cy.request(url + "clients").as("list");
});

When("send a post request with blank fields at the endpoint candidate", () => {
    cy.request({
        method: "POST",
        url: url + "client",
        body: BODY_CLIENT_BLANK,
    }).as("post");
});

When("send a post request at the endpoint client", () => {
    cy.request({
        method: "POST",
        url: url + "client",
        body: BODY_CLIENT,
    }).as("post");
});

Then("must visualize the clients correctly", () => {
    cy.get("@list").should((response) => {
        expect(response.status).to.eq(200);
    });
});

Then("should see the created client response correctly", () => {
    cy.get("@post").should((response) => {
        expect(response.status).to.eq(201);
        expect(response.body).has.property("created", true);
    });
});

Then("should see status and message error", () => {
    cy.get("@post").should((response) => {
        expect(response.status).to.eq(400);
        expect(response.body).has.property("created", false);
    });
});