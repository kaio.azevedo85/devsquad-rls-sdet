/* global Given, Then, When */

import CandidateListPage from "../pageobjects/candidateListPage";
const list = new CandidateListPage();


When("fill {string} in first name field correctly", (firstName) => {
    list.fillFirstNameField(firstName);
    list.clickFirstNameSearch()
});

When("fill {string} in last name field correctly", (firstName) => {
    list.fillLastNameField(firstName);
    list.clickLastNameSearch()
});

Then("should see total number of condidates", () => {
    list.compareTotalCandidates();
});

Then("should see the {string} candidate card correctly", (candidate) => {
    list.verifyCandidateResult(candidate);
});