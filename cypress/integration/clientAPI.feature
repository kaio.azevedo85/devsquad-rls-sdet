Feature: API Create client profile

    @get
    Scenario: Validate get clients
        When send a get request at the endpoint clients
        Then must visualize the clients correctly

    @post
    Scenario: Validate required fields in post body
        When send a post request with blank fields at the endpoint client
        Then should see status and message error

    @post
    Scenario: Validate new customer creation
        When send a post request at the endpoint client
        Then should see the created client response correctly