# CYPRESS
## SDET Real Life Tasks

### ** Why Cypress?**
Cypress is a next generation front end testing tool built for the modern web. We address the key pain points developers and QA engineers face when testing modern applications.

### **Bugs and Improvements**
[Trello Board](https://trello.com/invite/b/awwbUjrm/f14d6e449eb1286e880e4969bfe318ca/devsquad-board)

### **Test Plan**
[Test Plan](https://docs.google.com/document/d/1ygCQQa0_0w5aPUjuDYpMefSDroZIE0BxTmgKGKJCNww/edit?usp=sharing)

### **Documentation**

- [Setup](Setup.md)
- [Structure](Structure.md)
- [Execution](Execution.md)