/// <reference types="Cypress" />

import JobDescriptionElements from "../elements/jobDescriptionElements";
const job = new JobDescriptionElements();
let element = "";

class JobDescriptionPage {
    fillTitleField(jobTitle) {
        cy.get(job.jobTitle()).type(jobTitle);
    }
    fillDescriptionField(jobDescription) {
        cy.get(job.jobDescription()).type(jobDescription);
    }
    fillTechnicalKeywordsField(technicalKeywords) {
        cy.get(job.technicalKeywords()).type(technicalKeywords);
    }
    fillSoftSkillsField(softSkills) {
        cy.get(job.softSkills()).type(softSkills);
    }
    fillDueDateField(dueDate) {
        cy.get(job.dueDate()).type(dueDate);
    }
    fillNumberVacanciesField(numberVacancies) {
        cy.get(job.numberVacancies()).type(numberVacancies);
    }
    fillClientNameField(clientName) {
        cy.get(job.clientName()).type(clientName);
    }
    fillClientEmailField(clientEmail) {
        cy.get(job.clientEmail()).type(clientEmail);
    }
    fillClientBudgetField(clientBudget) {
        cy.get(job.clientBudget()).type(clientBudget);
    }
    selectSeniority(seniority) {
        cy.get(job.selectButton()).first().select(seniority);
    }
    selectStatus(status) {
        cy.get(job.selectButton()).eq(1).select(status);
    }
    selectClient() {
        cy.get(job.selectButton())
            .eq(2)
            .children()
            .eq(1)
            .then((element) =>
                cy.get(job.selectButton()).eq(2).select(element.val())
            );
    }
    clickCreateButton() {
        cy.contains(job.createButton()).click();
    }
    verifyCreatedSuccessfully() {
        cy.on("window:alert", (str) => {
            expect(str).to.equal("New job successfuly registered!");
        });
    }
    verifyRequiredFields() {
        cy.on("window:alert", (str) => {
            expect(str).to.equal(`Error: Request failed with status code 400`);
        });
    }
}

export default JobDescriptionPage;
