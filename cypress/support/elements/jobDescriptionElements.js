
class JobDescriptionElements {
    jobTitle = () => { return 'input[placeholder="Job title"]' }
    jobDescription = () => { return 'input[placeholder="Job description"]' }
    technicalKeywords = () => { return 'input[placeholder="Technical keywords"]' }
    softSkills = () => { return 'input[placeholder="Soft skills keywords"]' }
    dueDate = () => { return 'input[placeholder="Due date hiring"]' }
    numberVacancies = () => { return 'input[placeholder="Number of vacancies"]' }
    selectButton = () => { return 'select' }
    clientName = () => { return 'input[placeholder="Client contact name"]' }
    clientEmail = () => { return 'input[placeholder="Client contact email"]' }
    clientBudget = () => { return 'input[placeholder="Client budget per hour"]' }

    createButton = () => { return 'Create' }
}

export default JobDescriptionElements;
