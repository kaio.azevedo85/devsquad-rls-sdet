Feature: Create job description for client

    Scenario: Validate screen fields
        Given access to the RLS System website
        When access the New Job Description page
        Then must visualize the fields correctly

    @job
    Scenario: Validate required fields
        Given access to the RLS System website
        When access the "New Job Description" page
        And don't fill all the fields on the New Job Description page
        Then should see required fields error message

    @job
    Scenario: Validate new job creation
        Given access to the RLS System website
        When access the "New Job Description" page
        And fill in all fields correctly on the New Job Description page
        Then should see the created job description correctly