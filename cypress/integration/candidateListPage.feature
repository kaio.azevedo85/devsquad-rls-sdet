Feature: Candidates list page

    Scenario: Validate screen fields
        Given access to the RLS System website
        When access the "Candidates" page
        Then must visualize the card fields correctly

    @list
    Scenario: Validate candidate counts
        Given access to the RLS System website
        When access the "Candidates" page
        Then should see total number of condidates

    @list
    Scenario: Validate filter candidate by first name
        Given access to the RLS System website
        When access the "Candidates" page
        And fill "Jensen" in first name field correctly
        Then should see the "Jensen" candidate card correctly

    @list
    Scenario: Validate filter candidate by last name
        Given access to the RLS System website
        When access the "Candidates" page
        And fill "Jenkins" in last name field correctly
        Then should see the "Jenkins" candidate card correctly

    Scenario: Validate filter candidate by nonexistent candidate in first name
        Given access to the RLS System website
        When access the "Candidates" page
        And fill in first name field a nonexistent name
        Then should see a warning message

    Scenario: Validate filter candidate by nonexistent candidate in last name
        Given access to the RLS System website
        When access the "Candidates" page
        And fill in last name field a nonexistent name
        Then should see a warning message    

    Scenario: Validate pagination and limit candidates per page in page with less than 12 cadidates
        Given access to the RLS System website
        When access the "Candidates" page
        And fill in first name field correctly
        Then shouldn't see pagination

    Scenario: Validate pagination and limit candidates per page in page with more than 12 cadidates
        Given access to the RLS System website
        When access the "Candidates" page
        Then should see 12 candidates and pagination    