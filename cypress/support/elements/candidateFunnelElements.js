
class CandidateFunnelElements {
    fistStep = () => { return 'div.steps-container > header > h2' }
    candidates = () => { return 'div.steps-container > div.candidate-container' }
    nextStep = () => { return 'div.steps-container > div.candidate-container > div.candidate-header > button' }
}

export default CandidateFunnelElements;
