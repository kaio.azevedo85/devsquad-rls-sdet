
class CandidateListElements {
    inputName = () => { return 'input' }
    total = () => { return 'div.subheader-item > span' }
    searchButton = () => { return 'button' }
    candidatesList = () => {return 'div.profile-container'}
    cardName = () => {return 'div.profile-container > header > h2'}
}

export default CandidateListElements;
