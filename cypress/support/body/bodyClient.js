var BODY_CLIENT = {
  companyName: "Dunder Mifflin",
  companyWebsite: "http://www.dundermifflin.com/",
  companyLogoUrl:
    "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Dunder_Mifflin%2C_Inc.svg/1200px-Dunder_Mifflin%2C_Inc.svg.png",
  contactName: "David",
  contactPhone: "61 92222-4489",
  contactEmail: "david@dm.com",
};

var BODY_CLIENT_BLANK = {
  companyName: "",
  companyWebsite: "",
  companyLogoUrl: "",
  contactName: "",
  contactPhone: "",
  contactEmail: "",
};

module.exports = {
  BODY_CLIENT,
  BODY_CLIENT_BLANK,
};
