# Execution
<table>
<tr>
  <th>Navigate</th>
  <td>
      <a href="./../Setup.md">Setup</a> 
    | <a href="./../Structure.md">Structure</a>
  </td>
</tr>
</table>

## Index
- [Install Project Dependecies](#install-project-dependecies)
- [Execution](#execution)        
    * [Chrome Execution](#chrome-execution) 
    * [Firefox Execution](#firefox-execution) 
    * [Headless Execution](#headless-execution) 
    * [Tag Execution](#tag-execution) 
- [Tags](#tags)
- [Report](#cucumber-report)
            
## Install Project Dependecies

To install the project's dependencies, in the terminal navigate to `devsquad-rls-sdet` and run the command first
```shell script
npm install
```

## Execution
### Chrome Execution
```shell script
npm run test:chrome
```
### Firefox Execution
```shell script
npm run test:firefox
```
### Headless Execution
```shell script
npm run test
``` 
### Tag Execution
```shell script
./node_modules/.bin/cypress-tags run -e TAGS='@tag'
``` 
## Tags

To execute specific scenarios it is necessary to use "tags", as with them we can define groups of scenarios to be executed. 

## Cucumber Report
```shell script
node ./cypress/support/cucumber-html-reporter.js 
``` 
