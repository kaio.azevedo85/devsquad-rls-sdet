/// <reference types="Cypress" />

import CandidateElements from "../elements/candidateProfileElements";
const candidate = new CandidateElements();
let element = "";


class CandidateProfilePage {
    fillFirstNameField(firstName) {
        cy.get(candidate.firstName()).type(firstName);
    }
    fillLastNameField(lastName) {
        cy.get(candidate.lastName()).type(lastName);
    }
    fillEmailField(email) {
        cy.get(candidate.email()).type(email);
    }
    fillCellphoneField(cellphone) {
        cy.get(candidate.cellphone()).type(cellphone);
    }
    fillPhotoUrlField(photoUrl) {
        cy.get(candidate.photoUrl()).type(photoUrl);
    }
    fillRateHourField(rateHour) {
        cy.get(candidate.rateHour()).type(rateHour);
    }
    fillWorkingHoursField(workingHours) {
        cy.get(candidate.workingHours()).type(workingHours);
    }
    fillLinkedinUrlField(linkedinUrl) {
        cy.get(candidate.linkedinUrl()).type(linkedinUrl);
    }
    fillLinkedinTalentUrlField(linkedinTalentUrl) {
        cy.get(candidate.linkedinTalentUrl()).type(linkedinTalentUrl);
    }
    fillNotesField(notes) {
        cy.get(candidate.notes()).type(notes);
    }
    selectCurrency(currency) {
        cy.get(candidate.selectButton()).first().select(currency);
    }
    selectCurrencySign(currencySign) {
        cy.get(candidate.selectButton()).eq(1).select(currencySign);
    }
    selectJobDescription() {
        cy.get(candidate.selectButton())
            .eq(2)
            .children()
            .eq(1)
            .then((element) =>
                cy.get(candidate.selectButton()).eq(2).select(element.val())
            );
    }
    clickCreateButton() {
        cy.contains(candidate.createButton()).click();
    }
    verifyCreatedSuccessfully() {
        cy.on("window:confirm", (str) => {
            expect(str).to.equal("New candidate successfuly 111 registered!");
        });
    }
    verifyRequiredFields() {
        cy.on("window:alert", (str) => {
            expect(str).to.equal(`Error: Request failed with status code 400`);
        });
    }
}

export default CandidateProfilePage;
