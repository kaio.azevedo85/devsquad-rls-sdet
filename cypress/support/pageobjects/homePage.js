/// <reference types="Cypress" />

import HomeElements from '../elements/homeElements'
const homeElements = new HomeElements

const url = Cypress.config("baseUrl");

class HomePage {
    visitSite() {
        cy.visit(url);
    }

    clickPage(page) {
        cy.contains(page).click();
    }
}

export default HomePage;
