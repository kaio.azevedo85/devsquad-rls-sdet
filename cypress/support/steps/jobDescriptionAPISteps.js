const url = Cypress.config("apiUrl");

const { BODY_JOB, BODY_JOB_BLANK } = require("../body/bodyJob");

When("send a get request at the endpoint jobs", () => {
    cy.request(url + "jobs").as("list");
});

When("send a post request at the endpoint job", () => {
    cy.request({
        method: "POST",
        url: url + "job",
        body: BODY_JOB,
    }).as("post");
});

When("send a post request with blank fields at the endpoint job", () => {
    cy.request({
        method: "POST",
        url: url + "job",
        body: BODY_JOB_BLANK,
    }).as("post");
});

Then("must visualize the jobs correctly", () => {
    cy.get("@list").should((response) => {
        expect(response.status).to.eq(200);
    });
});

Then("should see the created job response correctly", () => {
    cy.get("@post").should((response) => {
        expect(response.status).to.eq(201);
        expect(response.body).has.property("created", true);
    });
});

Then("should see status and message error", () => {
    cy.get("@post").should((response) => {
        expect(response.status).to.eq(400);
        expect(response.body).has.property("created", false);
    });
});