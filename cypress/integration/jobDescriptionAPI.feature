Feature: API Job Description

    @get
    Scenario: Validate get jobs
        When send a get request at the endpoint jobs
        Then must visualize the jobs correctly

    @post
    Scenario: Validate required fields in post body
        When send a post request with blank fields at the endpoint job
        Then should see status and message error

    @post
    Scenario: Validate new job description creation
        When send a post request at the endpoint job
        Then should see the created job response correctly