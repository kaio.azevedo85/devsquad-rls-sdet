var BODY_JOB = {
  jobTitle: "Salesman",
  jobDescription: "Will sale some printers and paper",
  technicalKeywords: "Sales",
  softSkillsKeywords: "Great comumnication, agile",
  dueDateHiring: "16/12/20",
  numberVacancies: "5",
  seniorityLevel: "Middle",
  status: "Open",
  client: "Dunder Mifflin",
  clientContactName: "David",
  clientContactEmail: "david@dm.com",
  clientBudget: "$30/hour",
};

var BODY_JOB_BLANK = {
  jobTitle: "",
  jobDescription: "",
  technicalKeywords: "",
  softSkillsKeywords: "",
  dueDateHiring: "",
  numberVacancies: "",
  seniorityLevel: "",
  status: "",
  client: "",
  clientContactName: "",
  clientContactEmail: "",
  clientBudget: "",
};

module.exports = {
  BODY_JOB,
  BODY_JOB_BLANK,
};
