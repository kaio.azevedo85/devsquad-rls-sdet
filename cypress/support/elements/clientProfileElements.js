
class ClientProfileElements {
    companyName = () => { return 'input[placeholder="Company name"]' }
    companyWebsite = () => { return 'input[placeholder="Company website"]' }
    companyLogo = () => { return 'input[placeholder="Company logo url"]' }
    contactName = () => { return 'input[placeholder="Contact name"]' }
    contactPhone = () => { return 'input[placeholder="Contact phone"]' }
    contactEmail = () => { return 'input[placeholder="Contact email"]' }

    createButton = () => { return 'Create' }
}

export default ClientProfileElements;
