var BODY_CANDIDATE = {
  firstName: "Dwight",
  lastName: "Schrute",
  email: "dwight@mail.com",
  phone: "61 93284-3211",
  photoUrl:
    "https://media-exp1.licdn.com/dms/image/C5603AQGFW_HEE7t0hQ/profile-displayphoto-shrink_800_800/0/1559336073840?e=1613001600&v=beta&t=VvyGjQdgQheBCOkVPV0NpgKOE-HzlYheH8ieFHOtpwI",
  rate: "40",
  currencyName: "USD",
  currencySign: "$",
  workingHours: 8,
  linkedingPublicUrl: "https://www.linkedin.com/in/dwight-schrute-657971187/",
  linkedingTalentUrl: "https://www.linkedin.com/in/dwight-schrute-657971187/",
  notes: "Assistant of regional manager",
  jobDescription: "Manager"
};

var BODY_CANDIDATE_BLANK = {
  firstName: "",
  lastName: "",
  email: "",
  phone: "",
  photoUrl:
    "",
  rate: "",
  currencyName: "",
  currencySign: "",
  workingHours: 0,
  linkedingPublicUrl: "",
  linkedingTalentUrl: "",
  notes: "",
  jobDescription: ""
};

module.exports = {
  BODY_CANDIDATE,
  BODY_CANDIDATE_BLANK,
};
