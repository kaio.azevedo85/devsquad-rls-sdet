/// <reference types="Cypress" />

import CandidateListElements from "../elements/candidateListElements";
const list = new CandidateListElements();

class CandidateListPage {
	fillFirstNameField(firstName) {
		cy.get(list.inputName()).first().type(firstName);
	}
	fillLastNameField(lastName) {
		cy.get(list.inputName()).eq(1).type(lastName);
	}
	clickFirstNameSearch() {
		cy.get(list.searchButton()).eq(1).click();
	}
	clickLastNameSearch() {
		cy.get(list.searchButton()).eq(2).click();
	}
	compareTotalCandidates() {
		cy.get(list.total()).then(($total) => {
			let total = $total.text();
			cy.get(list.candidatesList()).should("have.length", total);
		});
	}
	verifyCandidateResult(name) {
		cy.get(list.cardName()).first().then(($cardName) => {
			let cardName = $cardName.text();
			expect(cardName).to.include(name);
		});
	}
}

export default CandidateListPage;
