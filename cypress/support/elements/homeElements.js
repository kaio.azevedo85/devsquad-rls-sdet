
class HomeElements {
    newClient = () => { return '#root > div > button:nth-child(2)' }
    newJob = () => { return '#root > div > button:nth-child(3)' }
    newCandidateProfile = () => { return '#root > div > button:nth-child(4)' }
    newCandidateFunnel = () => { return '#root > div > button:nth-child(5)' }
    candidates = () => { return '#root > div > button:nth-child(6)' }

}

export default HomeElements;
