/* global Given, Then, When */

import HomePage from "../pageobjects/homePage";
const home = new HomePage();

Given("access to the RLS System website", () => {
    home.visitSite();
});

When("access the {string} page", (page) => {
    home.clickPage(page);
});