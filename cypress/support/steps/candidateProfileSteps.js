/* global Given, Then, When */

import CandidateProfilePage from "../pageobjects/candidateProfilePage";
const candidate = new CandidateProfilePage();
const faker = require("faker");

var firstName = faker.name.firstName();
var lastName = faker.name.lastName();
var email = faker.internet.email();
var cellphone = faker.phone.phoneNumber();
var photoUrl = faker.image.avatar();
var rateHour = '40';
var workingHours = '40';
var linkedinUrl = faker.internet.url();
var linkedinTalentUrl = faker.internet.url();
var notes = faker.lorem.sentence();

When("don't fill all the fields on the New Candidate Profile page", () => {
	candidate.clickCreateButton();
});

When("fill in all fields correctly on the New Candidate Profile page", () => {
	candidate.fillFirstNameField(firstName);
	candidate.fillLastNameField(lastName);
	candidate.fillEmailField(email);
	candidate.fillCellphoneField(cellphone);
	candidate.fillPhotoUrlField(photoUrl);
	candidate.fillRateHourField(rateHour);
	candidate.fillWorkingHoursField(workingHours);
	candidate.fillLinkedinUrlField(linkedinUrl);
	candidate.fillLinkedinTalentUrlField(linkedinTalentUrl);
	candidate.fillNotesField(notes);
	candidate.selectCurrency('USD');	
	candidate.selectCurrencySign('$');
	candidate.selectJobDescription();
	candidate.clickCreateButton();
});

Then("should see the created candidate profile correctly", () => {
	candidate.verifyCreatedSuccessfully();
});
