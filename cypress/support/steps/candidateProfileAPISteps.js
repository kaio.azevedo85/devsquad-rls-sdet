const url = Cypress.config("apiUrl");

const { BODY_CANDIDATE, BODY_CANDIDATE_BLANK } = require("../body/bodyCandidate");

When("send a get request at the endpoint candidates", () => {
    cy.request(url + "candidates").as("list");
});

When("send a get request at the endpoint candidate by first name {string}", (firstName) => {
    cy.request(url + "candidate" + "?firstName=#{firsName}").as("list");
    let property = 'firstName'
    let name = firstName
});

When("send a get request at the endpoint candidate by last name {string}", (lastName) => {
    cy.request(url + "candidate" + "?lastName=#{lastName}").as("list");
    let property = 'lastName'
    let name = lastName
});

When("send a post request at the endpoint candidate", () => {
    cy.request({
        method: "POST",
        url: url + "candidate",
        body: BODY_CANDIDATE,
    }).as("post");
});

When("send a post request with blank fields at the endpoint candidate", () => {
    cy.request({
        method: "POST",
        url: url + "candidate",
        body: BODY_CANDIDATE_BLANK,
    }).as("post");
});

Then("must visualize the candidates correctly", () => {
    cy.get("@list").should((response) => {
        expect(response.status).to.eq(200);
    });
});

Then("must visualize the candidate correctly", () => {
    cy.get("@list").should((response) => {
        expect(response.status).to.eq(200);
        expect(response.body).to.have.property('#{property}', '#{name}')
    });
});

Then("should see the created candidate response correctly", () => {
    cy.get("@post").should((response) => {
        expect(response.status).to.eq(201);
        expect(response.body).has.property("created", true);
    });
});

Then("should see status and message error", () => {
    cy.get("@post").should((response) => {
        expect(response.status).to.eq(400);
        expect(response.body).has.property("created", false);
    });
});