Feature: Create Candidate Profile

    Scenario: Validate screen fields
        Given access to the RLS System website
        When access the New Candidate Profile page
        Then must visualize the fields correctly

    Scenario: Validate required fields
        Given access to the RLS System website
        When access the "New Candidate Profile" page
        And don't fill all the fields on the New Candidate Profile page
        Then should see required fields error message

    @candidate
    Scenario: Validate new customer creation
        Given access to the RLS System website
        When access the "New Candidate Profile" page
        And fill in all fields correctly on the New Candidate Profile page
        Then should see the created candidate profile correctly