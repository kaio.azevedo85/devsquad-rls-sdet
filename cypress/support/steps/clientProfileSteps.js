/* global Given, Then, When */

import NewClientPage from "../pageobjects/clientProfilePage";
import HomePage from "../pageobjects/homePage";

const home = new HomePage();
const newClient = new NewClientPage();
const faker = require("faker");

var companyName = faker.company.companyName();
var companyWebsite = faker.internet.url();
var companyLogo = faker.image.imageUrl();
var contactName = faker.name.firstName() + " " + faker.name.lastName();
var contactPhone = faker.phone.phoneNumber();
var contactEmail = faker.internet.email();


When("fill in all fields correctly on the New Client page", () => {
    newClient.fillCompanyField(companyName);
    newClient.fillCompanyWebsiteField(companyWebsite);
    newClient.fillCompanyLogoField(companyLogo);
    newClient.fillContactNameField(contactName);
    newClient.fillContactPhoneField(contactPhone);
    newClient.fillContactEmailField(contactEmail);
    newClient.clickCreateButton();
});

When("don't fill all the fields on the New Client page", () => {
    newClient.clickCreateButton();
});

Then("should see the created client correctly", () => {
    newClient.verifyCreatedSuccessfully();
});

Then("should see required fields error message", () => {
    newClient.verifyRequiredFields();
});

