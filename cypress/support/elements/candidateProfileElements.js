
class CandidateProfileElements {
    firstName = () => { return 'input[placeholder="First name"]' }
    lastName = () => { return 'input[placeholder="Last name"]' }
    email = () => { return 'input[placeholder="Email"]' }
    cellphone = () => { return 'input[placeholder="Cellphone/WhatsApp"]' }
    photoUrl = () => { return 'input[placeholder="Photo URL"]' }
    rateHour = () => { return 'input[placeholder="Rate per hour"]' }
    workingHours = () => { return 'input[placeholder="Working hours per day"]' }
    linkedinUrl = () => { return 'input[placeholder="Linkeding public profile URL"]' }
    linkedinTalentUrl = () => { return 'input[placeholder="Linkeding talent profile URL"]' }
    notes = () => { return 'input[placeholder="Notes"]' }
    selectButton = () => { return 'select' }

    createButton = () => { return 'Create' }
}

export default CandidateProfileElements;
