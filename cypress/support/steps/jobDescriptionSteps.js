/* global Given, Then, When */

import JobDescriptionPage from "../pageobjects/jobDescriptionPage";
const job = new JobDescriptionPage();
const faker = require("faker");


var jobTitle = faker.name.jobTitle();
var jobDescription = faker.name.jobDescriptor();
var technicalKeywords = faker.lorem.words(3);
var softSkills = faker.lorem.words(3);
var dueDate = '16/12/20';
var numberVacancies = '5';
var clientName = faker.name.firstName() + " " + faker.name.lastName();
var clientEmail = faker.internet.email();
var clientBudget = '$30/hour';

When("don't fill all the fields on the New Job Description page", () => {
	job.clickCreateButton();
});

When("fill in all fields correctly on the New Job Description page", () => {
	job.fillTitleField(jobTitle);
	job.fillDescriptionField(jobDescription);
	job.fillTechnicalKeywordsField(technicalKeywords);
	job.fillSoftSkillsField(softSkills);
	job.fillDueDateField(dueDate);
	job.fillNumberVacanciesField(numberVacancies);
	job.fillClientNameField(clientName);
	job.fillClientEmailField(clientEmail);
	job.fillClientBudgetField(clientBudget);
	job.selectSeniority('Senior');	
	job.selectStatus('Open');
	job.selectClient();
	job.clickCreateButton();
});

Then("should see the created job description correctly", () => {
	job.verifyCreatedSuccessfully();
});
